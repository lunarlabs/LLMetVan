extends "res://scripts/StateMachine.gd"

func _ready():
	add_state("idle")
	add_state("run")
	add_state("jump")
	add_state("double_jump")
	add_state("fall")
	call_deferred("set_state", states.idle)

func _input(event):
	if [states.idle, states.run].has(state):
		if event.is_action_pressed("jump"):
			parent.velocity.y = parent.max_jump_velocity
			parent.is_jumping = true
	#stop jump if jump released
	if state == states.jump:
		if event.is_action_released("jump") && parent.velocity.y > parent.min_jump_velocity:
			parent.velocity.y = parent.min_jump_velocity
	if [states.jump, states.fall].has(state) && !parent.is_double_jumping:
		if Input.is_action_just_pressed("jump") && parent.can_double_jump:
			parent.velocity.y = parent.max_jump_velocity
			parent.is_double_jumping = true

func _state_logic(delta):
	parent._handle_move_input()
	parent._apply_gravity(delta)
	parent._apply_movement()

func _get_transition(delta):
	match state:
		states.idle:
			if !parent.is_on_floor():
				if parent.velocity.y > 0:
					return states.jump
				elif parent.velocity.y > 0:
					return states.fall
			elif parent.velocity.x != 0:
				return states.run
		states.run:
			if !parent.is_on_floor():
				if parent.velocity.y > 0:
					return states.jump
				elif parent.velocity.y > 0:
					return states.fall
			elif parent.velocity.x == 0:
				return states.idle
		states.jump:
			if parent.is_on_floor():
				return states.idle
			elif parent.velocity.y <= 0:
				return states.fall
			elif parent.is_double_jumping:
				return states.double_jump
		states.double_jump:
			if parent.is_on_floor():
				return states.idle
			elif parent.velocity.y <= 0:
				return states.fall
		states.fall:
			if parent.is_on_floor():
				return states.idle
			elif parent.velocity.y > 0:
				return states.jump
	return null

func _enter_state(new_state, old_state):
	match new_state:
		states.idle:
			parent.is_double_jumping = false
			parent.animTree.set("parameters/PrimaryState/current", 0)
			parent.gun.visible = false
		states.run:
			parent.animTree.set("parameters/PrimaryState/current", 1)
			parent.gun.visible = false
		states.jump:
			parent.animTree.set("parameters/PrimaryState/current", 2)
			parent.gun.visible = false
		states.double_jump:
			parent.animTree.set("parameters/PrimaryState/current", 2)
			parent.gun.visible = false
		states.fall:
			parent.animTree.set("parameters/PrimaryState/current", 3)
			parent.gun.visible = false

func _exit_state(old_state, new_state):
	pass
