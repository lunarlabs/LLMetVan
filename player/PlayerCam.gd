extends Camera

export var targetOffset : Vector3
export var smooth = true
export var smoothSpeed = 0.125



func _process(delta):
	var Start = self.get_translation()
	if Start != targetOffset:
		if smooth:
			var Lerp = Start.linear_interpolate(targetOffset, smoothSpeed)
			self.set_translation(Lerp)
		else:
			self.set_translation(targetOffset)
