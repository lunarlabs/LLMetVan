extends PanelContainer

onready var label = $Label
onready var player = get_node("..")

var interactionTarget: Node

func changeLabel(input: String):
	label.text = input
