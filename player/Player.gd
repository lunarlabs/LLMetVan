extends KinematicBody

##Physics Stuff
const GRAVITY = GlobalVariables.GRAVITY
const GROUND_SPEED = 5.5
const JUMP_STRENGTH = 5
const SLOPE_STOP = 2
const DROP_THRU_BIT = 1
const ACCEL = 2.5
const DECEL = 15
const AIR_DECEL = 3
const SNAP = -0.1
const COYOTE = 0.15

##Signals
signal hp_change(health, oldhealth)
signal died()
signal max_health_change(max_health)
signal energy_change(energy)
signal max_energy_change(max_energy)
signal xp_change(xp)
signal energy_recharge_start(time)
signal energy_recharge_halt(value)

var max_jump_velocity
var min_jump_velocity
var max_jump_height = 3
var min_jump_height = 0.5
var jump_duration = 0.5

#States
var can_player_control = true

var is_jumping = false
var is_sliding = false
var can_use_energy = true

var can_double_jump = true

var interaction_ready = false

#var bullet = preload()

##Movement
var dir
var velocity = Vector3()

#Change this to handle stat changes later...
#var max_health = 60 setget _set_max_health
#var max_energy = 30

var max_health
var max_energy

var health setget _set_health
var energy

var facing = "RIGHT"
var fall_time = 0

var _snap

var TwoDHeadPoint = Vector2()

var bullet_scene = preload("res://player/PlayerBullet.tscn")

onready var animTree = $AnimationTree
onready var playerMesh = $playerCharacter
#onready var colShape = get_node("CollisionShape").shape
#onready var raycasts = $Raycasts
onready var debuginfo = $DebugInfo
#onready var sm = $StateMachine
#onready var health = max_health setget _set_health #TODO replace this with better handling
onready var invinc_timer = $InvulnTimer
onready var effects_ani = $FxAnim
onready var headpoint = $HeadPoint
onready var camera = $PlayerCam
onready var energy_tween = $EnergyTween
onready var recharge_timer = $RechargeTimer
#onready var hud = $HUD
onready var gun_timer = $FireTimer
onready var action_box = $ActionBox
onready var bullet_origin_h = $"playerCharacter/GunOrigin-Horiz"
onready var bullet_origin_u = $"playerCharacter/GunOrigin-Up"
onready var bullet_origin_d = $"playerCharacter/GunOrigin-Down"
onready var ray = $FloorRay

var state_machine_playback : AnimationNodeStateMachinePlayback

func _ready():
	max_health = GlobalVariables.stats.hp
	max_energy = GlobalVariables.stats.energy
	health = 1
	energy = max_energy
	move_lock_z = true
	max_jump_velocity = sqrt(2 * abs(GRAVITY) * max_jump_height)
	min_jump_velocity = sqrt(2 * abs(GRAVITY) * min_jump_height)
	state_machine_playback = animTree.get("parameters/playback")
	#print(str(get_children()))
	Console.connect_node(self)
	
func _physics_process(delta):
	if !can_use_energy and energy == max_energy:
		can_use_energy = true
	process_input(delta)
	process_movement(delta)
	process_animation()
	
func process_input(delta):
	dir = Vector3()
	_snap = Vector3(0, -0.1, 0)
	camera.targetOffset.y = 1.5	
		
	if can_player_control:
		if Input.is_action_pressed("move_right"):
			dir.x += 1
			camera.targetOffset.x = 3
		if Input.is_action_pressed("move_left"):
			dir.x -= 1
			camera.targetOffset.x = -3
			
		if Input.is_action_pressed("move_up") and dir.x == 0 and is_on_floor() and !interaction_ready:
			camera.targetOffset.y = 4
		
		if Input.is_action_just_pressed("move_up") and is_on_floor() and interaction_ready:
			can_player_control = false
			action_box.interactionTarget.interact()
		
		if Input.is_action_pressed("move_down") and dir.x == 0 and is_on_floor():
			camera.targetOffset.y = -2
		
		if Input.is_action_pressed("shoot") and gun_timer.is_stopped():
			if use_energy(1):
				gun_timer.start()
				process_gunfire()
		
		if !is_falling():
			if Input.is_action_just_pressed("jump"):
				_snap = Vector3(0,0,0)
				velocity.y = max_jump_velocity
				is_jumping = true
			else:
				is_jumping = false
				# if !can_double_jump:
				# 	can_double_jump = true
		
		# if !is_on_floor() && can_double_jump:
		# 	if Input.is_action_just_pressed("jump"):
		# 		velocity.y = max_jump_velocity
		# 		can_double_jump = false
		
		if !is_on_floor() && velocity.y > 0:
			if Input.is_action_just_released("jump"):
				velocity.y = min(min_jump_velocity, velocity.y)
			
	if dir.x < 0 and facing == "RIGHT":
		playerMesh.rotate_y(deg2rad(-180))
		facing = "LEFT"
	elif dir.x > 0 and facing == "LEFT":
		playerMesh.rotate_y(deg2rad(180))
		facing = "RIGHT"


func process_movement(delta):
	velocity.y += delta * GRAVITY
	
	var xvel = velocity
	xvel.y = 0
	
	var tgt = dir * GROUND_SPEED
	
	var accel
	if dir.dot(xvel) > 0:
		accel = ACCEL
	else:
		if is_on_floor():
			accel = DECEL
		else:
			accel = AIR_DECEL
	
	xvel = xvel.linear_interpolate(tgt, accel*delta)
	velocity.x = xvel.x
	
	if dir.dot(velocity) == 0:
		var _vel_clamp := 0.25
		if velocity.x < _vel_clamp and velocity.x > -_vel_clamp:
			velocity.x = 0
	
	velocity = move_and_slide(velocity, Vector3.UP, true)
	if is_on_floor():
		fall_time = 0
	else:
		fall_time += delta
	debuginfo.set_text("X: " + str(velocity.x) + "\nY: " + str(velocity.y) \
	+ "\nFall: " + str(fall_time) + "\nWall: " + str(is_on_wall()))
	
	TwoDHeadPoint = Vector2(camera.unproject_position(headpoint.to_global(Vector3.ZERO)))
	if action_box.visible:
		action_box.rect_position = TwoDHeadPoint + Vector2(-(action_box.rect_size.x/2),-32)

func process_animation():
	if !is_falling():
		if dir != Vector3.ZERO:
			state_machine_playback.travel("running")
		else:
			state_machine_playback.travel("standing")
	else:
		if velocity.y > 0:
			state_machine_playback.travel("jump")
		else:
			state_machine_playback.travel("fall")

func process_gunfire():
	var fire_animation
	match state_machine_playback.get_current_node():
		"standing":
			fire_animation = "parameters/standing/OneShot/active"
		"running":
			fire_animation = "parameters/running/OneShot/active"
		_:
			fire_animation = ""
	animTree.set(fire_animation, true)
	fire_gun()

func is_falling():
	return fall_time >= COYOTE

#Called from animations.
func fire_gun():
	var clone = bullet_scene.instance()
	var scene_root = get_node("/root/Main")
	clone.BULLET_DAMAGE = GlobalVariables.stats.attack
	scene_root.add_child(clone)
	
	if Input.is_action_pressed("move_up"):
		clone.global_transform = bullet_origin_u.global_transform
	elif Input.is_action_pressed("move_down"):
		clone.global_transform = bullet_origin_d.global_transform
	else:
		clone.global_transform = bullet_origin_h.global_transform

func fire_special(special_weapon):
	pass

#Player damage handler.
func damage(amount):
	if invinc_timer.is_stopped():
		invinc_timer.start()
		_set_health(health - amount)
		effects_ani.play("Damage")
		effects_ani.queue("Flash")

#General health setter, damage references this
func _set_health(value):
	var prev_health = health
	health = clamp(value, 0, max_health)
	if health != prev_health:
		emit_signal("hp_change", health, prev_health)
		if health == 0:
			#death handler
			emit_signal("died")

func _on_InvulnTimer_timeout():
	effects_ani.play("Rest")

#Call this function when you want to use an energy-spending move. It will return true when the energy can be and is spent, false otherwise.
func use_energy(amount) -> bool:
	if !can_use_energy:
		return false
	elif amount < energy:
		energy_tween.stop(self, "energy")
		energy = floor(energy)
		energy -= amount
		recharge_timer.start(2)
		return true
	else:
		energy_tween.stop(self, "energy")
		energy = 0
		can_use_energy = false
		recharge_timer.start(4)
		return true

func _energy_recharge(from_empty):
	if from_empty == false:
		energy_tween.interpolate_property(self, "energy", null, max_energy,
		lerp(0, 2, (max_energy - energy)/max_energy),Tween.TRANS_LINEAR)
		energy_tween.start()
	else:
		energy_tween.interpolate_property(self, "energy", null, max_energy,
		4, Tween.TRANS_LINEAR)
		energy_tween.start()

func _on_RechargeTimer_timeout():
	_energy_recharge(true if energy == 0 else false)

func _on_EnergyTween_tween_all_completed():
	can_use_energy = true

func max_health_change(amount):
	var difference = amount - max_health
	max_health = amount
	_set_health(health + difference)

func max_energy_change(amount):
	max_energy = amount
	energy = max_energy

func _on_Hitbox_area_entered(area):
	if area is Hitbox:
		if area.get_collision_layer_bit(7):
			damage(GlobalVariables.calculateDamage(area.damage, GlobalVariables.stats.defense, true))
		elif area.get_collision_layer_bit(6):
			var is_clear = (action_box.interactionTarget == null)
			assert(is_clear, "Interaction areas shouldn't intersect or interactionTarget wasn't cleared.")
			if is_clear:
				action_box.interactionTarget = area.entity
				action_box.rect_size = Vector2(0, 36)
				action_box.changeLabel(action_box.interactionTarget.action_label)
				action_box.rect_position = TwoDHeadPoint + Vector2(-(action_box.rect_size.x/2),-32)
				action_box.show()
				interaction_ready = true

func _on_Hitbox_area_exited(area):
	if area is Hitbox and area.get_collision_layer_bit(6):
		action_box.interactionTarget = null
		action_box.hide()
		action_box.changeLabel("")
		interaction_ready = false

## DEBUG COMMANDS ##
