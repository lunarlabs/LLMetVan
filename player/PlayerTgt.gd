extends Spatial

# Max and Min in the x axis
export var xMax = 0.0
export var yMax = 0.0
# Max and Min in the y axis
export var xMin = 0.0
export var yMin = 0.0
# Smooth Camera Trigger and SmoothSpeed value
export var Smooth = false
export var SmoothSpeed = 0.125

var player

func _ready():
	set_process(false)

func activate():
	player = GlobalVariables.player
	set_process(true)

func _process(delta):
	var Target = player.get_translation()
	var x_modifier = 2 if player.facing == "RIGHT" else -2
	var Own = self
	
	var Start = self.get_translation()
	
	var Clampx = clamp(Target.x+x_modifier, xMin, xMax)
	var Clampy = clamp(Target.y+1.5, yMin, yMax)
	var Clampz = Own.get_translation().z
	
	var All = Vector3(Clampx, Clampy, Clampz)
	
	if Smooth == false:
		self.set_translation(All)
	if Smooth == true:
		var Lerp = Start.linear_interpolate(All, SmoothSpeed)
		self.set_translation(Lerp)

func set_x_range(a, b):
	xMin = min(a, b)
	xMax = max(a, b)
	
func set_y_range(a, b):
	yMin = min(a, b)
	yMax = max(a, b)


func _on_Main_new_camera_extents(xmin, xmax, ymin, ymax):
	pass # Replace with function body.
