extends Area

signal entity_damaged(entity)

export (NodePath) var parent_path = ".."
onready var parent = get_node(parent_path)

export (float) var damage_amount = 1

var exceptions = []

var hit_something = false

func add_exception(node : Node):
	exceptions.append(node)
	
func remove_exception(node : Node):
	exceptions.erase(node)

func _on_DamageArea_area_entered(area):
	if area is Hitbox and !hit_something:
		if !exceptions.has(area.entity) && area.entity.has_method("damage"):
			area.entity.damage(damage_amount, self)
	hit_something = true
