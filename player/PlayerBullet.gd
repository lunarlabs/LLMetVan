extends Spatial

var BULLET_SPEED = 15
var BULLET_DAMAGE

const KILL_TIMER = 4
var timer = 0

var hit_something = false

func _physics_process(delta):
	var forward_dir = global_transform.basis.z.normalized()
	global_translate(forward_dir * BULLET_SPEED * delta)

	timer += delta
	if timer >= KILL_TIMER:
		queue_free()

func _on_DamageArea_body_entered(body):
	if !hit_something:
		if body.has_method("damage"):
			body.damage(BULLET_DAMAGE)
	hit_something = true
	queue_free()
