extends StateMachine

func _ready():
	add_state("none")
	add_state("fire")
	call_deferred("set_state", states.none)

func _get_transition(delta):
	match state:
		states.none:
			if Input.is_action_pressed("shoot") && can_fire():
				return states.fire

func _enter_state(new_state, old_state):
	match new_state:
		states.none:
			pass
			
func can_fire() -> bool:
	return false
