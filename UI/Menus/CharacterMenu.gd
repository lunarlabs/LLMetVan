extends PanelContainer

onready var boxes = {
	"hp": $HBoxContainer/Stats/HP,
	"energy": $HBoxContainer/Stats/Energy,
	"attack": $HBoxContainer/Stats/Attack,
	"defense": $HBoxContainer/Stats/Defense,
	"luck": $HBoxContainer/Stats/Luck
}
onready var level = $HBoxContainer/TextureRect/VBoxContainer/HBoxContainer/Level
onready var XP = $HBoxContainer/TextureRect/VBoxContainer/XPBar/XP
onready var PtsToAssign = $HBoxContainer/TextureRect/VBoxContainer/HBoxContainer2/PtsToAssign
onready var XPBar = $HBoxContainer/TextureRect/VBoxContainer/XPBar

func _on_Delta_visibility_changed():
	if self.visible:
		_update_boxes()
		level.text = str(GlobalVariables.xpLevel)
		XPBar.max_value = GlobalVariables.xpToNext
		XPBar.value = GlobalVariables.xp
		XP.text = str(GlobalVariables.xp) + "/" \
		+ str(GlobalVariables.xpToNext)
		PtsToAssign.text = str(GlobalVariables.pointsToAssign)

func _update_boxes():
	for i in boxes:
			boxes[i].setAmount(floor(GlobalVariables.base[i] 
			* GlobalVariables.multipliers[i]) 
			+ GlobalVariables.bonuses[i])
			boxes[i].setLevel(GlobalVariables.statLevels[i])

func _upgrade_stat(stat):
	GlobalVariables.statLevels[stat] += 1
	
	pass
