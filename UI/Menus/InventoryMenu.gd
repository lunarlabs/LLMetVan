extends PanelContainer

onready var item_list = $HBoxContainer/VBoxContainer/ItemList
onready var money_label = $HBoxContainer/VBoxContainer/HBoxContainer/MoneyCounter
onready var equipped_items = {
	"head": $HBoxContainer/EquipmentList/HeadItem,
	"body": $HBoxContainer/EquipmentList/BodyItem,
	"feet": $HBoxContainer/EquipmentList/FeetItem,
	"accessory": $HBoxContainer/EquipmentList/AccessoryItem,
	"quick": $HBoxContainer/EquipmentList/QuickItem
}

func _on_Inventory_visibility_changed():
	if self.visible:
		money_label.text = str(GlobalVariables.money)
		item_list.clear()
		_update_inventory_list()

func _update_inventory_list():
	for i in range(GlobalVariables.inventory.STACKS.size()):
		item_list.add_item("x" + str(GlobalVariables.inventory.at_slot(i).stackSize))

#single click
func _on_ItemList_item_selected(index):
	pass # Replace with function body.

#double click
func _on_ItemList_item_activated(index):
	pass # Replace with function body.
