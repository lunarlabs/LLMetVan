extends Node

enum {
	ARG_INT,
	ARG_STRING,
	ARG_BOOL,
	ARG_FLOAT
}

var valid_commands = {
	"set_level": [ARG_INT],
	"what_is": [ARG_STRING],
	"list": [ARG_STRING]
}

func _ready():
	pass

#GENERAL COMMANDS
func list(type):
	if type == "commands":
		return str("Commands: ", valid_commands.keys())
	elif type == "items":
		return str("Items: ", GDInv_ItemDB.REGISTRY.keys())
	else:
		return "list: commands, items"

#INVENTORY
func what_is(item):
	if item in GDInv_ItemDB.REGISTRY:
		return str(GDInv_ItemDB.REGISTRY[item].attributes)
	else:
		return str(item, " doesn't exist")

#LEVELS AND EXPERIENCE
func set_level(lvl):
	lvl = int(lvl)
	
	if lvl >= 0 and lvl <= 50:
		return str("Level set to ", lvl)
	else:
		return "set_level: Level range is 0 - 50"

