extends HBoxContainer

signal statAdvance

onready var amount = $Amount
onready var prog = $ProgressBar
onready var adv = $Advance

func setAmount(a):
	amount.text = str(a)

func setLevel(a):
	prog.value = a
	
func turnOnActivate(tf):
	adv.disabled = !tf

func _on_Advance_pressed():
	emit_signal("statAdvance")
