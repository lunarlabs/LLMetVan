extends Control

onready var globalAnims = $GlobalAnimations
onready var updateTween = $Top/UpperLeft/BarsBG/UpdateTween
onready var hpBarUnder = $Top/UpperLeft/BarsBG/HPBarUnder
onready var hpBarOver =$Top/UpperLeft/BarsBG/HPBarUnder/HPBarOver
onready var hpCounter =$Top/UpperLeft/BarsBG/HP/CurrentHP
onready var maxHPCounter =$Top/UpperLeft/BarsBG/HP/MaxHP
onready var energyBar =$Top/UpperLeft/BarsBG/EnBar
onready var energyAnimation = $Top/UpperLeft/BarsBG/EnBar/EnergyAnimations
onready var xpBar = $Top/UpperLeft/BarsBG/XPBar
onready var moneyCounter = $Top/UpperLeft/MoneyCounter
onready var moneyCounterAmount = $Top/UpperLeft/MoneyCounter/amount
onready var messagePanel = $Top/MessagePanel
onready var missionIndicator = $Indicators/MissionUpdate
onready var levelUpIndicator = $Indicators/LevelUp
onready var player = GlobalVariables.player
# Called when the node enters the scene tree for the first time.

func _ready():
	hpCounter.text = str(player.health)
	hpBarUnder.max_value = player.max_health
	hpBarOver.max_value = player.max_health
	energyBar.max_value = player.max_energy
	hpBarOver.value = player.health
	hpBarUnder.value = player.health
	maxHPCounter.text = str(player.max_health)
	energyBar.value = player.energy
	xpBar.max_value = GlobalVariables.xpToNext
	xpBar.value = GlobalVariables.xp

func _process(delta):
	if energyBar.value != player.energy:
		process_energy()
	if energyBar.value == energyBar.max_value && energyAnimation.current_animation != "idle":
		energyAnimation.play("idle")
#func zoom(zoomIn):
#	var zoomedOut = Vector2(2.0, 2.0)
#	var zoomedIn =  Vector2(1.0, 1.0)
#	if(zoomIn):
#		tween.interpolate_property(self, rect_scale, zoomedOut, zoomedIn, 0.5, Tween.TRANS_SINE, Tween.EASE_OUT)
#		if not tween.is_active():
#			tween.start()

func process_energy():
	if player.can_use_energy == false && energyAnimation.current_animation != "recharge":
		energyAnimation.play("recharge")
	elif player.energy <= player.max_energy * 0.5 && energyAnimation.current_animation != "low" && player.can_use_energy:
		energyAnimation.play("low")
	if player.energy == 0:
		energyAnimation.play("empty")
	energyBar.value = player.energy

func _set_center():
	rect_pivot_offset = rect_size / 2

func _on_Player_hp_change(health, oldhealth):
	hpCounter.text = str(health)
	hpBarOver.value = health
	if health < oldhealth:
		updateTween.interpolate_property(hpBarUnder, "value", hpBarUnder.value, \
		health, 0.3,Tween.TRANS_SINE,Tween.EASE_IN_OUT)
		updateTween.start()
	else:
		hpBarUnder.value = health

func _on_Player_max_health_change(max_health):
	maxHPCounter.text = str(max_health)
	hpBarUnder.max_value = max_health
	hpBarOver.max_value = max_health
