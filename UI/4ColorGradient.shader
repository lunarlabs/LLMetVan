shader_type canvas_item;
uniform vec4 top_left : hint_color;
uniform vec4 top_right : hint_color;
uniform vec4 bottom_left : hint_color;
uniform vec4 bottom_right : hint_color;

void fragment() {
	vec4 top = mix(top_left, top_right, UV.x);
	vec4 bot = mix(bottom_left, bottom_right, UV.x);
	COLOR = mix(top, bot, UV.y);
}