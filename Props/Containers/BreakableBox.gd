extends StaticBody

export (int, "Item", "Money", "Nothing") var ContainedItem
export (String) var ItemID
export (int) var MoneyValue

onready var collisionShape = $CollisionShape
onready var debris_scene = preload("res://Props/Containers/BreakableBoxDebris.tscn")
onready var scene_root = get_node("/root/Main")
var item
var debris

func _ready():
	debris = debris_scene.instance()
	match ContainedItem:
		0:
			pass
		1:
			pass
		2:
			pass

func damage(_dummy):
	collisionShape.disabled = true
	scene_root.add_child(debris)
	debris.global_transform = global_transform
	queue_free()
