extends RigidBody

export (int, 1, 200) var value = 1

onready var dummy = $Sprite3D
onready var collectionArea = $CollectionArea
var meshInstance
var collision

func _ready():
	dummy.visible = false
	assert(value > 0, "Money pickup must have a non-zero positive value.")
	if value < 10:
		meshInstance = $Rotation/SmallCrystal
		collision = $SmallCollision
	elif value < 50:
		meshInstance = $Rotation/MedCrystal
		collision = $MedCollision
	else:
		meshInstance = $Rotation/BigCrystal
		collision = $BigCollision
	
	meshInstance.visible = true
	collision.disabled = false

func _on_CollectionArea_body_entered(body):
	collectionArea.monitoring = false
	print(str("touched ", value))
	GlobalVariables.money += value
	queue_free()
