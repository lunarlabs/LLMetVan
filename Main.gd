extends Node

onready var playerScn = preload("res://player/Player.tscn")
onready var hudScn = preload("res://UI/HUD.tscn")
#onready var gameCam = $GameCamera
#onready var playerTgt = $PlayerTgt
onready var inventoryMenu = $InvMenu

#onready var mainMenu
var player
var hud
var level

var stored_health

var currentLevel

var input_disabled

#signal new_camera_extents(xmin,xmax,ymin,ymax)

func _ready():
	print("MetroidVania Project")
	print("LunarLabs, 2019-2020")
	GlobalVariables.inventory = $GDInv_Inventory
	Console.pause_mode = PAUSE_MODE_PROCESS
	Console.connect("visibility_changed", self, "_on_Console_visibility_changed")
	
	#stuff for loading the main menu goes here.
	startNewGame()
#	for i in range(11):
#		print(str(GlobalVariables.calculateStat(i,60,4500)))

func startNewGame():
	currentLevel = load("res://Levels/Testing/slopetest.tscn")
	level = currentLevel.instance()
	add_child(level)
	GlobalVariables.recalc_stats()
	player = playerScn.instance()
	add_child(player)
	GlobalVariables.player = player
#	playerTgt.activate()
	player.set_translation(level.PlayerStart)
	player.health = player.max_health
	GlobalVariables.xpLevel = 0
	GlobalVariables.xp = 0
	GlobalVariables.xpToNext = 1000
	GlobalVariables.money = 0
#	for i in player.stats:
#		player.base[i] = GlobalVariables.EXTREMES[i][0]
	hud = hudScn.instance()
	add_child(hud)
	GlobalVariables.hud = hud

func _load_level(path):
	if level == null:
		pass
	else:
		printerr(str("Tried loading ", path, " when a level was already loaded."))

func _unload_level():
	if level != null:
		pass
	else:
		printerr("No level to unload!")

func _on_Player_hp_change(delta):
	pass # Replace with function body.

func _on_Console_visibility_changed():
	get_tree().paused = Console.visible

func _input(event):
	if !input_disabled:
		if Input.is_action_just_pressed("inventory") && !inventoryMenu.visible:
			player.action_box.modulate = Color(1,1,1,0)
			get_tree().paused = true
			#hud.hide()
			hud.globalAnims.play("FlyOut")
			inventoryMenu.show()
		elif inventoryMenu.visible && (Input.is_action_just_pressed("ui_cancel") \
			|| Input.is_action_just_pressed("inventory")):
			#hud.show()
			inventoryMenu.hide()
			hud.globalAnims.play("FlyIn")
			get_tree().paused = false
			player.action_box.modulate = Color(1,1,1,1)
			GlobalVariables.recalc_stats()
