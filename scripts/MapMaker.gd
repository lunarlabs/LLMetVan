extends Node

export (String, FILE, "*.tscn") var target
onready var viewport = $Viewport
onready var camera = $Viewport/Camera
onready var capture = $Capture
var boundingBox

func _ready():
	var scene = load(target)
	if scene == null: #there wasn't a scene in this file.
		printerr("No valid scene present.")
		get_tree().quit()
	else:
		var level = scene.instance()
		viewport.add_child(level)
		
		#wait until a frame's drawn
		yield(VisualServer, "frame_post_draw")
		get_tree().paused = true;
		#Now we want an array of all VisualInstances in the loaded scene
		var list = getAllDescendants(level)
		list = trimToVisual(list)
		print(str(list))
		
		#Get the AABB of the whole array and move/size the camera accordingly.
		boundingBox = AABB()
		
		for N in list:
			var append = N.get_aabb()
			boundingBox = boundingBox.merge(append)
		
		var center = boundingBox.position.linear_interpolate(boundingBox.end, 0.5)
		
		camera.set_translation(Vector3(center.x, center.y, 10))
		camera.size = boundingBox.get_longest_axis_size() * 1.25
		
		var unit = camera.unproject_position(Vector3(1,0,0)).x - \
		camera.unproject_position(Vector3.ZERO).x
		print(str(unit, " pixels per 3D unit"))

func getAllDescendants(node, result := []):
	for N in node.get_children():
		result.append(N)
		if N.get_child_count() > 0:
			getAllDescendants(N, result)
	return result

func trimToVisual(list):
	var result = []
	for N in list:
		if N is GeometryInstance:
			result.append(N)
	return result
