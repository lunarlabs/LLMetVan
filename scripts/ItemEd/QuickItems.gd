extends EdPanel

onready var hpBox = $Holder/RightSide/HPBox
onready var hpPerLabel = $Holder/RightSide/HpPerLbl
onready var spFXMenu = $Holder/RightSide/SpFxMenu
onready var bonusStatMenu = $Holder/RightSide/BonusStatMenu
onready var multiplierBox = $Holder/RightSide/MultiplierBox
onready var timeBox = $Holder/RightSide/TimeBox

func _ready():
	newItemTemplate.type = "quick_item"
	newItemTemplate.hp_restore = 0
	newItemTemplate.specialFX = "none"

func _on_ItemList_item_selected(index):
	._on_ItemList_item_selected(index)
	hpBox.value = itemDict[selectedID].hp_restore
	hpBox.editable = true
	calculate_hp_per(itemDict[selectedID].buy, itemDict[selectedID].hp_restore)
	match itemDict[selectedID].specialFX:
		"none":
			spFXMenu.select(0)
		"bonus":
			spFXMenu.select(1)
			match itemDict[selectedID].bonusStat:
				"hp":
					bonusStatMenu.select(0)
				"energy":
					bonusStatMenu.select(1)
				"attack":
					bonusStatMenu.select(2)
				"defense":
					bonusStatMenu.select(3)
				"luck":
					bonusStatMenu.select(4)
				"regen":
					bonusStatMenu.select(5)
			multiplierBox.value = itemDict[selectedID].bonusMult
			timeBox.value = itemDict[selectedID].bonusTime
		"burn_heal":
			spFXMenu.select(2)
		"antidote":
			spFXMenu.select(3)
		"unlock":
			spFXMenu.select(4)
	_on_SpFxMenu_item_selected(spFXMenu.selected)
	spFXMenu.disabled = false

func _on_BuyBox_value_changed(value):
	._on_BuyBox_value_changed(value)
	calculate_hp_per(value, itemDict[selectedID].hp_restore)

func _on_HPBox_value_changed(value):
	itemDict[selectedID].hp_restore = value
	calculate_hp_per(itemDict[selectedID].buy, value)
	saveButton.disabled = false

func _on_SpFxMenu_item_selected(index):
	match index:
		0:
			itemDict[selectedID].specialFX = "none"
			special_fx_block_changeState(false)
		1:
			itemDict[selectedID].specialFX = "bonus"
			special_fx_block_changeState(true)
		2:
			itemDict[selectedID].specialFX = "burn_heal"
			special_fx_block_changeState(false)
		3:
			itemDict[selectedID].specialFX = "antidote"
			special_fx_block_changeState(false)
		4:
			itemDict[selectedID].specialFX = "unlock"
			special_fx_block_changeState(false)
	saveButton.disabled = false

func _on_BonusStatMenu_item_selected(index):
	match index:
		0:
			itemDict[selectedID].bonusStat = "hp"
		1:
			itemDict[selectedID].bonusStat = "energy"
		2:
			itemDict[selectedID].bonusStat = "attack"
		3:
			itemDict[selectedID].bonusStat = "defense"
		4:
			itemDict[selectedID].bonusStat = "luck"
		5:
			itemDict[selectedID].bonusStat = "regen"
	saveButton.disabled = false

func _on_MultiplierBox_value_changed(value):
	itemDict[selectedID].bonusMult = value
	saveButton.disabled = false

func _on_TimeBox_value_changed(value):
	itemDict[selectedID].bonusTime = value
	saveButton.disabled = false

func calculate_hp_per(value, hp):
	hpPerLabel.text = str(value/hp)

func special_fx_block_changeState(activated:bool):
	if activated:
		bonusStatMenu.disabled = false
		multiplierBox.editable = true
		timeBox.editable = true
		itemDict[selectedID].bonusStat = "hp"
		itemDict[selectedID].bonusMult = 1
		itemDict[selectedID].bonusTime = 1
	else:
		bonusStatMenu.disabled = true
		multiplierBox.editable = false
		timeBox.editable = false
		bonusStatMenu.selected = 0
		multiplierBox.value = 1
		timeBox.value = 1
		itemDict[selectedID].erase("bonusStat")
		itemDict[selectedID].erase("bonusMult")
		itemDict[selectedID].erase("bonusTime")
