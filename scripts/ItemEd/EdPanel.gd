extends PanelContainer
class_name EdPanel

export (String, DIR) var directory_to_check

var newItemTemplate = {
	"name": "New Item",
	"maxStackSize": 99,
	"description": "A new item.",
	"buy": 1,
	"sell": 1,
	"iconx": 0,
	"icony": 0
}

var itemDict = {}
var selectedID: String

onready var itemList = $Holder/LeftSide/ItemList
onready var saveButton = $Holder/LeftSide/SaveFileButton

onready var idField = $Holder/RightSide/IDField
onready var nameField = $Holder/RightSide/NameField
onready var descField = $Holder/RightSide/DescField
onready var buyBox = $Holder/RightSide/BuyBox
onready var sellBox = $Holder/RightSide/SellBox
onready var xBox = $Holder/RightSide/IconBox/XBox
onready var yBox = $Holder/RightSide/IconBox/YBox
onready var iconPreview = $Holder/RightSide/IconBox/TextureRect

onready var newItemDlg = $NewItemDlg
onready var newItemNameBox = $NewItemDlg/NewItemBox
onready var alreadyExistslbl = $NewItemDlg/AlreadyExistsWarning

func _ready():
	print("Loading in...")
	#load in the items
	_read_in(directory_to_check)

	#populate the itemlist
	for i in itemDict:
		#print(i)
		itemList.add_item(i)

func _on_AddFileButton_pressed():
	newItemDlg.popup_centered()
	alreadyExistslbl.visible = false
	newItemNameBox.text = ""

func _on_NewItemCancelButton_pressed():
	newItemDlg.visible = false
	alreadyExistslbl.visible = false
	newItemNameBox.text = ""

func _on_ItemList_item_selected(index):
	selectedID = itemList.get_item_text(index)
	idField.text = selectedID
	nameField.text = itemDict[selectedID].name
	nameField.editable = true
	descField.text = itemDict[selectedID].description
	descField.editable = true
	buyBox.value = itemDict[selectedID].buy
	buyBox.editable = true
	sellBox.value = itemDict[selectedID].sell
	sellBox.editable = true
	xBox.value = itemDict[selectedID].iconx
	xBox.editable = true
	yBox.value = itemDict[selectedID].icony
	yBox.editable = true
	generate_item_image(itemDict[selectedID].iconx, itemDict[selectedID].icony)

func _on_NewItemBox_text_entered(new_text):
	if itemDict.has(new_text):
		newItemNameBox.text = ""
		alreadyExistslbl.text = "An item with that ID already exists."
		alreadyExistslbl.visible = true
	elif new_text == "":
		newItemNameBox.text = ""
		alreadyExistslbl.text = "ID can't be empty."
		alreadyExistslbl.visible = true
	else:
		itemDict[new_text] = newItemTemplate.duplicate(true)
		itemList.add_item(new_text)
		itemList.select(itemList.get_item_count()-1)
		_on_ItemList_item_selected(itemList.get_selected_items()[0])
		saveButton.disabled = false
		newItemDlg.visible = false
		alreadyExistslbl.visible = false
		newItemNameBox.text = ""

func _on_SaveFileButtons_pressed():
	save_item_list(directory_to_check)
	saveButton.disabled = true

func _on_NameField_text_entered(new_text):
	itemDict[selectedID].name = new_text
	saveButton.disabled = false

func _on_DescField_text_entered(new_text):
	itemDict[selectedID].description = new_text
	saveButton.disabled = false

func _on_BuyBox_value_changed(value):
	var sugg_sell = max(int(value/4), 1)
	itemDict[selectedID].buy = value
	itemDict[selectedID].sell = sugg_sell
	sellBox.value = sugg_sell
	saveButton.disabled = false

func _on_SellBox_value_changed(value):
	itemDict[selectedID].sell = value
	saveButton.disabled = false

func _on_XBox_value_changed(value):
	itemDict[selectedID].iconx = value
	generate_item_image(itemDict[selectedID].iconx, itemDict[selectedID].icony)
	saveButton.disabled = false

func _on_YBox_value_changed(value):
	itemDict[selectedID].icony = value
	generate_item_image(itemDict[selectedID].iconx, itemDict[selectedID].icony)
	saveButton.disabled = false

func _read_in(path: String):
	var dir = Directory.new()
	print(str("Now fetching from ", path))
	if dir.open(path) == OK:
		dir.list_dir_begin(true)
		var file_name = dir.get_next()
		
		while file_name != "":
			if (!dir.current_is_dir() && file_name.ends_with(".json")):
				print("   ", file_name, "...")
				_fetch_item(path + file_name)
			file_name = dir.get_next()
		print("done.")

func _fetch_item(location: String):
	var file = File.new()

	if (!file.file_exists(location)):
		print("    Doesn't exist.");
		return;
	
	if (file.open(location, File.READ)):
		print("    Can't open.");
		return;

	var json_res = JSON.parse(file.get_as_text())
	file.close()

	if json_res.error != 0:
		print ("   error: ", json_res.error)
		return
	
	var rawjson: Dictionary = json_res.result
	var item_id: String = rawjson.get("id")

	if item_id == null:
		print("    malformed: no 'id' string.")
		return
	
	if typeof(item_id) != TYPE_STRING:
		print("    malformed: 'id' field not a string.")
		return
	
	var attributes = rawjson.get("attributes", {})

	if typeof(attributes) != TYPE_DICTIONARY:
		print("    malformed: 'attributes' not a map.")
		return
	
	itemDict[item_id] = attributes

func save_item_list(path: String):
	var dir = Directory.new()
	
	print("writing to ", path)
	if dir.open(path) == OK:
		for i in itemDict:
			var item_file = File.new()
			var to_write = {}

			print("   ", i, "...")
			if item_file.open(path + i + ".json", File.WRITE):
				print("    Can't open.")
				continue
			
			to_write.id = i
			to_write.attributes = itemDict[i]

			var string_out = to_json(to_write)
			print(string_out)
			item_file.store_line(string_out)

			item_file.close()
	else:
		print("Can't open the directory.")

func generate_item_image(x_cell: int, y_cell: int):
	var tex : AtlasTexture = iconPreview.texture
	var x_loc = x_cell * 150
	var y_loc = y_cell * 150
	tex.region=Rect2(x_loc, y_loc, 150, 150)

