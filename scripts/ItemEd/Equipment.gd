extends EdPanel

onready var spFXMenu = $Holder/RightSide/SpFxMenu
onready var eqAsMenu = $Holder/RightSide/EquipAsMenu
onready var regenBox = $Holder/RightSide/RegenBox
onready var statBoxes = {
	"hp_multiplier": $Holder/RightSide/PanelContainer/GridContainer/HpMultBox,
	"hp_bonus": $Holder/RightSide/PanelContainer/GridContainer/HpBonusBox,
	"energy_multiplier": $Holder/RightSide/PanelContainer/GridContainer/EnMultBox,
	"energy_bonus": $Holder/RightSide/PanelContainer/GridContainer/EnBonusBox,
	"attack_multiplier": $Holder/RightSide/PanelContainer/GridContainer/AtkMultBox,
	"attack_bonus": $Holder/RightSide/PanelContainer/GridContainer/AtkBonusBox,
	"defense_multiplier": $Holder/RightSide/PanelContainer/GridContainer/DefMultBox,
	"defense_bonus": $Holder/RightSide/PanelContainer/GridContainer/DefBonusBox,
	"luck_multiplier": $Holder/RightSide/PanelContainer/GridContainer/LuckMultBox,
	"luck_bonus": $Holder/RightSide/PanelContainer/GridContainer/LuckBonusBox,
}

func _ready():
	newItemTemplate.type = "equipment"
	newItemTemplate.equipAs = "head"
	newItemTemplate.modifiers = {
		"hp_multiplier": 1,
		"energy_multiplier": 1,
		"attack_multiplier": 1,
		"defense_multiplier": 1,
		"luck_multiplier": 1,
		"hp_bonus": 0,
		"energy_bonus": 0,
		"attack_bonus": 0,
		"defense_bonus": 0,
		"luck_bonus": 0,
	}
	newItemTemplate.regen = 0
	newItemTemplate.specialFX = "none"

func set_stat_block_enable(on_off: bool):
	for i in statBoxes:
		statBoxes[i].editable = on_off

func _on_ItemList_item_selected(index):
	._on_ItemList_item_selected(index)
	match itemDict[selectedID].specialFX:
		"none":
			spFXMenu.select(0)
		"fire_immune":
			spFXMenu.select(1)
		"lock_immune":
			spFXMenu.select(2)
	match itemDict[selectedID].equipAs:
		"head":
			eqAsMenu.select(0)
		"body":
			eqAsMenu.select(1)
		"gunmod":
			eqAsMenu.select(2)
		"accessory":
			eqAsMenu.select(3)
	for i in statBoxes:
		statBoxes[i].value=itemDict[selectedID].modifiers[i]
	regenBox.value = itemDict[selectedID].regen
	regenBox.editable = true
	set_stat_block_enable(true)
	spFXMenu.disabled = false

func _on_SpFxMenu_item_selected(index):
	match index:
		0:
			itemDict[selectedID].specialFX = "none"
		1:
			itemDict[selectedID].specialFX = "fire_immune"
		2:
			itemDict[selectedID].specialFX = "lock_immune"
	saveButton.disabled = false

func _on_EquipAsMenu_item_selected(index):
	match index:
		0:
			itemDict[selectedID].equipAs = "head"
		1:
			itemDict[selectedID].equipAs = "body"
		2:
			itemDict[selectedID].equipAs = "gunmod"
		3:
			itemDict[selectedID].equipAs = "accessory"
	saveButton.disabled = false

## STAT BLOCK CHANGESS

func _on_HpMultBox_value_changed(value):
	itemDict[selectedID].modifiers.hp_multiplier = value
	saveButton.disabled = false

func _on_EnMultBox_value_changed(value):
	itemDict[selectedID].modifiers.energy_multiplier = value
	saveButton.disabled = false

func _on_AtkMultBox_value_changed(value):
	itemDict[selectedID].modifiers.attack_multiplier = value
	saveButton.disabled = false

func _on_DefMultBox_value_changed(value):
	itemDict[selectedID].modifiers.defense_multiplier = value
	saveButton.disabled = false

func _on_LuckMultBox_value_changed(value):
	itemDict[selectedID].modifiers.luck_multiplier = value
	saveButton.disabled = false

func _on_HpBonusBox_value_changed(value):
	itemDict[selectedID].modifiers.hp_bonus = value
	saveButton.disabled = false

func _on_EnBonusBox_value_changed(value):
	itemDict[selectedID].modifiers.energy_bonus = value
	saveButton.disabled = false

func _on_AtkBonusBox_value_changed(value):
	itemDict[selectedID].modifiers.attack_bonus = value
	saveButton.disabled = false

func _on_DefBonusBox_value_changed(value):
	itemDict[selectedID].modifiers.defense_bonus = value
	saveButton.disabled = false

func _on_LuckBonusBox_value_changed(value):
	itemDict[selectedID].modifiers.luck_bonus = value
	saveButton.disabled = false

func _on_RegenBox_value_changed(value):
	itemDict[selectedID].regen = value
	saveButton.disabled = false
