extends ColorRect

onready var quick_items = $MarginContainer/TabContainer/QuickItems
onready var equipment = $MarginContainer/TabContainer/Equipment
onready var materials = $MarginContainer/TabContainer/Materials
onready var saving_all_warning = $SavingAllWarning

func _ready():
	get_tree().set_auto_accept_quit(false)

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		saving_all_warning.popup_centered()
		quick_items.save_item_list(quick_items.directory_to_check)
		equipment.save_item_list(equipment.directory_to_check)
		materials.save_item_list(materials.directory_to_check)
		get_tree().quit()
