extends Spatial
class_name Level

export (Vector3) var PlayerStart = Vector3(0,0,0) setget _set_player_start, _get_player_start

func _set_player_start(value):
	PlayerStart = value

func _get_player_start():
	return PlayerStart
