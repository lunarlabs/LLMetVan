extends Area
class_name Hitbox

export (NodePath) var entity_path = ".."
export (int, 1, 9999) var damage = 1
onready var entity = get_node(entity_path)
var ready := true
