extends KinematicBody
class_name Enemy

signal hp_change(health, oldhealth)
signal died()

export (int) var maxHealth = 1 setget _set_max_health, _get_max_health
export (int) var attack = 1 setget _set_atk, _get_atk
export (int) var defense = 1 setget _set_defense, _get_defense

#onready var invinc_timer = Timer.new()
onready var health = maxHealth;

func _set_max_health(value):
	maxHealth = value
	health = min(health, maxHealth)
	
func _get_max_health():
	return maxHealth
	
func _set_atk(value):
	attack = value

func _get_atk():
	return attack
	
func _set_defense(value):
	defense = value

func _get_defense():
	return defense

func damage(amount):
	if health > 0:
		var dmg = GlobalVariables.calculateDamage(amount, defense)
		health -= dmg
		print(str("hit for", dmg))
		if health <= 0:
			emit_signal("died")
