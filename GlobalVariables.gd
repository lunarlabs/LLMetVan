extends Node

#Physics Constants
const GRAVITY = -18

#Stat Constants
const MAX_LEVEL = 50
const EXP_MULT = 1.16
const EXP_TO_FIRST = 1000

enum StatType {HP, ATTACK, DEFENSE, ENERGY, LUCK}
enum ItemType {MATERIAL, QUICK, HEAD, BODY, FEET, ACCESSORY}

#const HP_EXTREMES = [60, 4500]
#const ATK_EXTREMES = [5, 1000]
#const DEF_EXTREMES = [0, 675]
#const ENG_EXTREMES = [30, 3000]
#const LUCK_EXTREMES = [100, 200]

const EXTREMES = {
	"hp": [60, 4500],
	"energy": [30, 3000],
	"attack": [5, 1000],
	"defense": [1, 675],
	"luck": [100, 200]
}

#Global References
var player
var money = 0 setget _change_money
var inventory
var xp = 0 setget _change_xp
var xpToNext = EXP_TO_FIRST
var pointsToAssign = 0
var xpLevel = 0
var item_atlas_texture
var hud

#player stats
var stats = {
	"hp": 0,
	"energy": 0,
	"attack": 0,
	"defense": 0,
	"luck": 0
}

var statLevels = {
	"hp": 0,
	"energy": 0,
	"attack": 0,
	"defense": 0,
	"luck": 0
}

var base = {
	"hp": 1,
	"energy": 1,
	"attack": 1,
	"defense": 1,
	"luck": 1
}

var multipliers = {
	"hp": 1,
	"energy": 1,
	"attack": 1,
	"defense": 1,
	"luck": 1
}

var bonuses = {
	"hp": 0,
	"energy": 0,
	"attack": 0,
	"defense": 0,
	"luck": 0
}

var equipment = {
	"head": null,
	"body": null,
	"feet": null,
	"accessory": null,
	"quick": null
}

func _ready():
	item_atlas_texture = load("res://Items/images/atlas.png")
	Console.connect_node(self)

## INVENTORY AND MONEY ##
func _change_money(new_value):
	if new_value > money && hud != null:
		if hud.globalAnims.current_animation == "":
			hud.globalAnims.play("getMoney")
		elif hud.globalAnims.current_animation == "getMoney":
			hud.globalAnims.seek(0)
		else:
			hud.globalAnims.queue("getMoney")
	
	money = new_value
	if hud != null:
		hud.moneyCounterAmount.text = str(money)

## STATS AND XP ##

func _change_xp(new_value):
	xp = new_value

#Calculates the XP needed to advance to level level. We're using the range 0-50 here.
func calculateExpToLevel(level):
	assert (level > 1 && level < MAX_LEVEL) #Catch illegal uses before we can do any damage
	return floor(EXP_TO_FIRST * (EXP_MULT^(level - 1)))

#So we can just ask for "what's the number for this stat at this level?"
func calculateStat(stat, level):
	return calculateCurve(level, EXTREMES[stat][0], EXTREMES[stat][1])

#Normally, this shouldn't be called from outside GlobalVariables.
func calculateCurve(level, minVal, maxVal):
	if level <= 0:
		return minVal
	elif level >= 10:
		return maxVal
	else:
		return floor(((0.8 + level)/8) * level * ((maxVal-minVal)/13.5) + minVal + 0.1)
		
func recalc_stats():
	var prev_health = stats.hp
	var prev_energy = stats.energy
	for i in stats:
		base[i] = calculateStat(i, statLevels[i])
		stats[i] = floor(base[i] * multipliers[i]) + bonuses[i]
	if player != null:
		if stats.hp != prev_health:
			player.max_health_change(stats.hp)
		if stats.energy != prev_energy:
			player.max_energy_change(stats.energy)

## DAMAGE SYSTEM ##

#Calculates damage given to an entity, given attack and defense (and optional power multiplier)
func calculateDamage(attack, defense, from_enemy := false, power = 1) -> float:
	defense = max(defense, 1)
	var bonus
	if from_enemy:
		bonus = 2 if (randi() % int(16 * (stats.luck / 100) == 0)) else 1
	else:
		bonus = 2 if (randi() % 16 < (stats.luck / 100)) else 1
	return floor(0.5 * power * (attack/defense) * rand_range(0.85, 1.0) * bonus)\
	+ 1

## DEBUG COMMANDS ##
